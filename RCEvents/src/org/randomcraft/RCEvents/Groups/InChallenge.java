package org.randomcraft.RCEvents.Groups;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class InChallenge implements Listener{

	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String chatprefix = (ChatColor.GRAY + "[" + ChatColor.RESET + "Challenge" + ChatColor.GRAY + "] ");
	public static ArrayList<Player> ic = new ArrayList<Player>();
	
	public static ArrayList<Player> getIC(){
		return ic;
	}
	
	public void onBuild(BlockPlaceEvent event) {
		
		Player player = event.getPlayer();
		
		if(!player.hasPermission("randomcraft.challenge.bypass")) {
			if(ic.contains(player)) {
				event.setCancelled(true);
				player.sendMessage(prefix + "You are not allowed to do that!");
				return;
			}else {
				event.setCancelled(false);
				return;
			}
		}else {
			event.setCancelled(false);
			return;
		}
	}
	
	public void onBreak(BlockBreakEvent event) {
		
		Player player = event.getPlayer();
		
		if(!player.hasPermission("randomcraft.challenge.bypass")) {
			if(ic.contains(player)) {
				event.setCancelled(true);
				player.sendMessage(prefix + "You are not allowed to do that!");
				return;
			}else {
				event.setCancelled(false);
				return;
			}
		}else {
			event.setCancelled(false);
			return;
		}
	}
	
	public void onChat(AsyncPlayerChatEvent event) {
		
		Player player = event.getPlayer();
		
		if(!player.hasPermission("randomcraft.challenge.bypass")) {
			if(ic.contains(player)) {
				event.setCancelled(true);
				
				for(Player ic : ic) {
					ic.sendMessage(prefix + chatprefix + player.getDisplayName() + ChatColor.GRAY + " � " + ChatColor.RESET + event.getMessage());
					Bukkit.getServer().getConsoleSender().sendMessage(prefix + chatprefix + player.getDisplayName() + ChatColor.GRAY + " � " + ChatColor.RESET + event.getMessage());
					return;
				}
			}else {
				event.setCancelled(false);
				return;
			}
		}
		
		
	}
	
}
