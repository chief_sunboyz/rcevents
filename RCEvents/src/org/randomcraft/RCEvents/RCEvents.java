package org.randomcraft.RCEvents;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.randomcraft.RCEvents.Commands.EventStart;
import org.randomcraft.RCEvents.Commands.LobbyJoin;
import org.randomcraft.RCEvents.Commands.Solve;
import org.randomcraft.RCEvents.Groups.InChallenge;
import org.randomcraft.RCEvents.Groups.SpecChallenge;

public class RCEvents extends JavaPlugin implements Listener{

	
	
	
	@SuppressWarnings("unused")
	public void onEnable() {
		
		PluginManager pm =Bukkit.getServer().getPluginManager();
		pm.registerEvents(this, this);
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		
		EventStart es = new EventStart(this);
		Solve solve = new Solve();
		LobbyJoin lbj = new LobbyJoin();
		
		
		getServer().getConsoleSender().sendMessage(ChatColor.DARK_BLUE + "------------------------");
		getServer().getConsoleSender().sendMessage(ChatColor.BLUE + "RCEvents Has been Enabled!");
		getServer().getConsoleSender().sendMessage(ChatColor.DARK_BLUE + "------------------------");
		
		getServer().getPluginManager().registerEvents(new InChallenge(), this);
		getServer().getPluginManager().registerEvents(new SpecChallenge(), this);
		getServer().getPluginManager().registerEvents(new EventStart(this), this);

		
		getCommand("event").setExecutor(es);
		getCommand("solve").setExecutor(solve);
		getCommand("eventjoin").setExecutor(lbj);

		
		getConfig().options().copyDefaults(true);
		saveConfig();
		reloadConfig();
	}
	
	
	public void onDisable() {
		
		
		getServer().getConsoleSender().sendMessage(ChatColor.DARK_BLUE + "------------------------");
		getServer().getConsoleSender().sendMessage(ChatColor.BLUE + "RCEvents Has been Disabled!");
		getServer().getConsoleSender().sendMessage(ChatColor.DARK_BLUE + "------------------------");
		
	}
	
}
