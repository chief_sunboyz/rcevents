package org.randomcraft.RCEvents.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEvents.RCEvents;
import org.randomcraft.RCEvents.Groups.InChallenge;
import org.randomcraft.RCEvents.Groups.SpecChallenge;

import net.minecraft.server.v1_8_R3.CommandExecute;

public class ChallengeStartCopy extends CommandExecute implements Listener, CommandExecutor{

	private RCEvents plugin;
	public ChallengeStartCopy(RCEvents instance) {
	    plugin = instance;
	}
	
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	
	public ArrayList<Player> ic = InChallenge.getIC();

	public ArrayList<Player> sc = SpecChallenge.getSC();
	public ArrayList<Player> solved = Solve.getSolved();
	public ArrayList<Player> winner = Solve.getWinner();

	
	@SuppressWarnings("unlikely-arg-type")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		
		if(sender.hasPermission("randomcraft.challenge.start") && sender.hasPermission("randomcraft.challenge.end")) {
			
			
			if(cmd.getName().equalsIgnoreCase("challenge")) {
				if(args.length >= 1) {
					
					if(args[0].equalsIgnoreCase("start")) {
						for (String key : plugin.getConfig().getConfigurationSection("Challenges").getKeys(false)) {
							
							System.out.println(key);

						if(args[1].equalsIgnoreCase(key)) {
							
							String challenge = plugin.getConfig().getString("Challenges." + args[1]);
							
							Bukkit.getServer().broadcastMessage(prefix + "A challenge is starting in 1 minute! Do /event join " + plugin.getConfig().getString(challenge + ".eventlobby.world") + " to join!");
							 
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

								@Override
								public void run() {
									Bukkit.getServer().getConsoleSender().sendMessage(prefix + "A challege has started in " + plugin.getConfig().getString(challenge + ".eventlobby.world") + "!");
									
										for(Player player : Bukkit.getOnlinePlayers()) {
											if(player.getWorld().equals(plugin.getConfig().getString(challenge + ".eventlobby.world"))) {
											
												ic.add(player);
												player.setHealth(20);
												player.setFoodLevel(20);
																				
												
												
												
												String world = plugin.getConfig().getString(challenge + ".maplocation.orld");
												
												World worldvalue = Bukkit.getServer().getWorld(world);
												float yaw = plugin.getConfig().getInt(challenge + ".yaw");
												float pitch = plugin.getConfig().getInt(challenge + ".pitch");
												float x = plugin.getConfig().getInt(challenge + ".x");
												float y = plugin.getConfig().getInt(challenge + ".y");					
												float z = plugin.getConfig().getInt(challenge + ".z");
												
												Location map = new Location(worldvalue, x ,y, z, yaw, pitch);
												player.teleport(map);												
												
												
												return;
											
											
											
										}
											return;
									}	
										return;
								}
								
							}, 1200);
							
							

						
					}else if(args[1] != key) {
						System.out.println(key);
						sender.sendMessage(prefix + "Invalid Arguments!");
						return false;
					}
				}if(args[0].equalsIgnoreCase("end")) {
					
					String challenge = plugin.getConfig().getString("Challenges." + args[1]);
					
					if(args[1].equalsIgnoreCase(plugin.getConfig().getString(challenge + ".startendcommand"))) {
						Bukkit.getServer().broadcastMessage(prefix + "A challenge is ending in 1 minute!");
						
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

							@Override
							public void run() {
								
								Bukkit.getServer().broadcastMessage(prefix + "A challenge has ended and " + winner.listIterator().toString() + " won!");
								
								for(Player player : Bukkit.getOnlinePlayers()) {
									if(ic.contains(player)) {
										
										ic.remove(player);
										sc.remove(player);
										
										
										
										player.setHealth(20);
										player.setFoodLevel(20);
										player.performCommand("hub");
										

										//Current: Sends a message to staff with who won
										
										if(player.hasPermission("randomcraft.challenge.results")) {
											player.sendMessage(prefix + winner.listIterator().toString() + "won the challenge, and " + solved.listIterator().toString() + " solved the challenge!");
											
											return;
										}
									}else {
										return;
									}
								}									
							}
							
						}, 1200);
					}else {
						sender.sendMessage(prefix + "Invalid Arguments!");
						return true;
					}
				}else {
					
					return true;
				}
			}else {
				sender.sendMessage(prefix + "Invalid Arguments!");
				return true;
			}
		}
		
		return true;
		
		}else {
			sender.sendMessage(prefix + "You do not have permission!");
			return true;
		}
		}
		return false;
	}

}
